package domain;

public class Order {
    private long id;
    private Pizza pizza;
    private String userToppings;
    private String address;
    private double totalPrice;

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUserToppings() {
        return userToppings;
    }

    public void setUserToppings(String userToppings) {
        this.userToppings = userToppings;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", pizza=" + pizza +
                ", userToppings='" + userToppings + '\'' +
                ", address='" + address + '\'' +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
