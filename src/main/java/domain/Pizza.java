package domain;

import java.util.List;

public class Pizza {
    private String name;
    private List<Toppings> toppings;
    private double cost;

    public Pizza() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Toppings> getToppings() {
        return toppings;
    }

    public void setToppings(List<Toppings> toppings) {
        this.toppings = toppings;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void addTopping(Toppings topping) {
        toppings.add(topping);
        cost += 10;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "name='" + name + '\'' +
                (!toppings.isEmpty() ? ", toppings=" + toppings : "") +
                ", cost=" + cost +
                "uah}";
    }
}
