import domain.Order;
import domain.Pizza;
import domain.Toppings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@WebServlet("/delivery")
public class DeliveryServlet extends HttpServlet {
    private static Logger log = LogManager.getLogger(DeliveryServlet.class);
    private List<Toppings> toppings = Arrays.asList(Toppings.values());
    private List<Pizza> pizzas = new ArrayList<Pizza>();
    private List<Order> orders = new ArrayList<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("doGet is invoked");
        req.setAttribute("toppings", toppings);
        req.setAttribute("pizzas", pizzas);
        req.getRequestDispatcher("delivery.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("doPost is invoked");
        String pizzaName = req.getParameter("pizzaName");
        Pizza pizza = pizzas.stream().filter(p -> p.getName().equalsIgnoreCase(pizzaName)).findFirst().get();
        String userToppings = req.getParameter("userToppings");
        double totalPrice = new Double(req.getParameter("totalPrice"));
        String address = req.getParameter("address");
        Order newOrder = new Order();
        newOrder.setId(orders.size());
        newOrder.setPizza(pizza);
        newOrder.setUserToppings(userToppings);
        newOrder.setAddress(address);
        newOrder.setTotalPrice(totalPrice);
        orders.add(newOrder);
        log.info(newOrder + " -- is added");
    }

    @Override
    public void init() throws ServletException {
        log.info("Start initialization...");
        Pizza base = new Pizza();
        base.setName("Base");
        base.setCost(40);
        Pizza pizza1 = new Pizza();
        Pizza pizza2 = new Pizza();
        Pizza pizza3 = new Pizza();
        pizza1.setName("Pepperoni");
        pizza1.setToppings(Arrays.asList(new Toppings[]{Toppings.blue_cheese, Toppings.pepper, Toppings.ham}));
        pizza1.setCost(80.0);
        pizza2.setName("Cheese");
        pizza2.setToppings(Arrays.asList(new Toppings[]{Toppings.blue_cheese, Toppings.parmigiana, Toppings.tomato}));
        pizza2.setCost(99.9);
        pizza3.setName("Margarita");
        pizza3.setToppings(Arrays.asList(new Toppings[]{Toppings.mozzarella, Toppings.tomato, Toppings.onion, Toppings.olives}));
        pizza3.setCost(65.5);
        pizzas.add(base);
        pizzas.add(pizza1);
        pizzas.add(pizza2);
        pizzas.add(pizza3);
        log.info("Success finished initialization...");
    }
}
